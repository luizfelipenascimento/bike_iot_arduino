/***********************************************************************
 * 
 *  ROBO EQUILIBRISTA ESCOLA KPACITOR
 *  
 *  MINICURSO: CONSTRUINDO UM ROBÔ EQUILIBRISTA
 * 
 *  WWW.KPACITOR.COM
 *  
 *  OUTUBRO DE 2017
 *  
 *  elderlucas@kpacitor.com
 *  
 *  
 ************************************************************************/

#include <Wire.h>
#include <Kalman.h>
#include <SoftwareSerial.h>


//INDENTIFICADORES - TODO E QUALQUER IDENTIFICADOR DEVE SER FORMADO POR UMA PALAVRA OU LETRA SEGUIDO POR ":"(DOIS PONTOS). EX.: "A:" , "C:" , "IDENT:"

//indicadores para requisicao
//R = indica um erro durante a requisicao, esse erro é enviado ao solicitante. Ocorre quando o arduino recebe alguma coisa, porém não é uma requisicao valida (algo diferente de SENDIT)
//K = indica que a requisição é valida, essa informação é enviada ao solicitante logo apos o recebimento da requisicao. (requisição = SENDIT) 

const char ERROR_REQUEST_INDICATOR = 'R';
const char SUCCESS_REQUEST_INDICATOR = 'K';


//identificadores das informacoes que serao enviadas
const String GYROSCOPE_INFO_INDICATOR = "G:";
const String ACCELEROMETER_INFO_INDICATOR = "A:";
const String HALL_EFFECT_INFO_INDICATOR = "H:";
const String ROLL_INFO_INDICATOR = "O:";
const String PITCH_INFO_INDICATOR = "P:";

//identificadores dos eixos: para acelerometro e giroscopio
const String Z_INDEX = "z:";
const String X_INDEX = "x:";
const String Y_INDEX = "y:";

//definicao das regras do pacote que sera enviado para o bluetooth

const String COMMA_SEPARATOR = ",";  //caso o sensor tenha um conjunto de dados atrelado a ele mesmo, e necessario utilizar a "," (virgula) para separar cada dado do sensor
                                    //como exemplo podemos pensar no sensor de acelerometro que possui 3 eixos XYZ - a informação ficaria: X,Y,Z (uso da virgula separando cada dado)  

const String END_INFO_INDICATOR = ";"; //final de cada conjuto de dados de cada sensor

const String START_PACKEGE_DATA = "DATA:"; // indicador do inicio do pacote que sera enviado SEMPRE UTILIZAR NO INICIO DO PACOTE (E A PRIMEIRA INFORMACAO QUE PRECISA SER ENVIADA)

const String END_PACKEGE_DATA = "}"; //indicador do final do pacote que sera enviado FINALIZADOR - SEMPRE UTILIZAR NO FINAL DO PACOTE (E A ULTIMA INFORMACAO QUE PRECISA SER ENVIADA)

//EXEMPLO DE PACOTES COMPLETOS COM DADOS: DATA:G:12,12,5;A:4,5,6;H:5;}

/*EXPLICACAO DETALHADA DE CADA PARTE DO PACOTE:
  
  DATA:  //(inicio do pacote)
  G:12,12,5; //(dados de um sensor que possui 3 eixos ) Identificador = G: / Eixos separados por = "," (virgula) / Finalizacao do conjuto de dados desse sensor =  ";" (ponto e virgula)
  A:4,5,6; //(dados de um sensor que possui 3 eixos ) Identificador = A: / Eixos separados por = "," (virgula) / Finalizacao do conjuto de dados desse sensor =  ";" (ponto e virgula)
  H:5;  //(dado de um sensor que possui somente 1 valor atralado a ele) Identificador = H: / valor / Finalizacao do conjuto de dados desse sensor = ";" (ponto e virgula)
  } // (finalizador do pacote)

*/

//REGRA DE COMUNICACAO COM BLUETOOTH 
//O ARDUINO SO IRA ENVIAR ALGUMA INFORMACAO PARA O CELULAR VIA BLUETOOTH CASO ELE TENHA RECEBIDO UMA REQUISICAO
// ESSA REQUISICAO E CARACTERIZADA POR UMA PALAVRA-CHAVE = (SENDIT). O ARDUINO IRA IDENTIFICAR QUE ESSA PALAVRA FOI ENVIADA
// E IRA COMECAR A PREPARAR OS DADOS PARA ENVIO.

 //TRATAMENTO DE ERROS DURANTE A REQUISICAO 
 //CASO O ARDUINO RECEBA ALGUMA INFORMACAO, MAS QUE NAO SEJA A PALAVRA-CHAVE (SENDIT) O ARDUINO IRA ENVIAR A LETRA (R) 
 // DE VOLTA AO SOLICITANTE INDICANDO QUE OCORREU UM ERRO DURANTE A REQUISICAO 

 //CASO O ARDUINO RECEBA A PALAVRA-CHAVE (SENDIT) CORRETAMENTE O ARDUINO IR ENVIAR A LETRA (K) DE VOLTA AO SOLICITANTE INDICANDO QUE A REQUISICAO FOI
 //RECEBIDA COM SUCESSO

 //RESUMO DAS PALAVRAS-CHAVE UTILIZADAS DURANTE A COMUNICACAO (TODAS AS PALAVRAS SAO MAIUSCULAS)
 /*
  SENDIT = utilizada para requisicao de dados no arduino, um solicitante precisa enviar essa palavra para que o arduino possa responder com os dados

  R = indica um erro durante a requisicao, esse erro é enviado ao solicitante. Ocorre quando o arduino recebe alguma coisa, porém não é uma requisicao valida (algo diferente de SENDIT)

  K = indica que a requisição é valida, essa informação é enviada ao solicitante logo apos o recebimento da requisicao. (requisição = SENDIT) 
 
 */


uint8_t i2c_data[14];
double accX, accY, accZ;
double gyroX, gyroY, gyroZ;

uint32_t timer;

Kalman KalmanX;
Kalman KalmanY;
Kalman KalmanZ;

double KalAngleX;
double KalAngleY;
double KalAngleZ;

double gyroXangle;
double gyroYangle;

const long INTERVALO_SEGUNDOS = 1000 * 1;

//Contador do sensor efeito hall
int pino_hall = 2;
int valor = 1;
static int count = 0;

long tempoInicial;
long tempoFinal;
int completedCount = 0;


//inverter nos testes que serao realizados com a shield - 11, 10
SoftwareSerial bluetooth(10,11);


/************************************************************************
 * Setup Inicial com as Configurações para i2C.
 ************************************************************************/

void setup() {

  /* Inicializando a Serial para exibir mensagens de Debug */
  Serial.begin(9600);

  /*Inicializando bluetooth */
  bluetooth.begin(9600);
  
  /* Inicializando o Barramento i2c para comunicação com a MPU6050 */
  Wire.begin();

#if ARDUINO >= 157
  Wire.setClock(400000UL); // Freq = 400kHz.
#else
  TWBR = ((F_CPU/400000UL) - 16) / 2; // Freq = 400kHz
#endif

  /* 1 - Leitura dos dados de Acc XYZ */
  /* 2 - Organizar os dados de Acc XYZ */
  accX = 22;
  accY = 23;
  accZ = 24;

  /* 3 - Calculo de Pitch e Roll */  
  double pitch = atan(accX/sqrt(accY * accY + accZ * accZ)) * RAD_TO_DEG;
  double roll = atan(accY/sqrt(accX * accX + accZ * accZ)) * RAD_TO_DEG;

  /* 4 - Inicialização do Filtro de Kalman XY */
  KalmanX.setAngle(roll);
  KalmanY.setAngle(pitch);

  gyroXangle = roll;
  gyroYangle = pitch;
  
  timer = micros();
    
  tempoInicial = millis();
  tempoFinal = millis();
}

void loop() {

  String request = "";

  if(bluetooth.available()) {
    while(bluetooth.available()) {
      char ch = bluetooth.read();
      request += ch;
      delay(10);
    }
    //verificando a requisicao
    if (request == "SENDIT") {
      //leitura do sensor efeito hall com intervalo
      //Serial.println("while para check de tempo: ");
      while((tempoFinal - tempoInicial) <= INTERVALO_SEGUNDOS + 500) {
      
        tempoFinal = millis();
        
        if (tempoFinal - tempoInicial >= INTERVALO_SEGUNDOS) {
          
          //a contangem é sempre acrescentada quando o estado do sensor e modificado (ligar ou desligar) para ter somente o valor de um estado count foi dividido por 2
          completedCount = count / 2;
          Serial.println("contador: " + String(completedCount)); 
          
          count = 10; //testing
          tempoInicial = millis();  
          break;
        }
      }
      
      /* Leitura dos Dados de Aceleração e Gyro do sensor MPU6050 */
    
    
      /*Aceleração*/
      accX = 22;
      accY = 23;
      accZ = 21;
    
      /*Giroscópio*/
      gyroX = 30;
      gyroY = 31;
      gyroZ = 35;
    
      /******************* Filtro de Kalman *************************/
      
      /* Calculo do Delta Time */
      double dt = (double)(micros() - timer)/1000000;
      timer = micros();
    
      double pitch = atan(accX/sqrt(accY * accY + accZ * accZ)) * RAD_TO_DEG;
      double roll = atan(accY/sqrt(accX * accX + accZ * accZ)) * RAD_TO_DEG;
    
      /* Convertendo de Rad/Segundo para Graus/Segundo Calculo da Taxa angular baseado no Giroscópio */
      gyroXangle = gyroX / 131.0; //deg/s
      gyroYangle = gyroY / 131.0; //deg/s
    
      /* Estimativa de Ângulo nos Eixos X e Y usando Filtro de Kalman */
      KalAngleX = KalmanX.getAngle(roll, gyroXangle, dt);
      KalAngleY = KalmanY.getAngle(pitch, gyroYangle, dt);

      String girosInfo = GYROSCOPE_INFO_INDICATOR + X_INDEX + String(gyroX) + COMMA_SEPARATOR + Y_INDEX + String(gyroY) + COMMA_SEPARATOR + Z_INDEX + String(gyroZ) + END_INFO_INDICATOR;
      String acceInfo = ACCELEROMETER_INFO_INDICATOR + X_INDEX + String(accX) + COMMA_SEPARATOR + Y_INDEX + String(accY) + COMMA_SEPARATOR + Z_INDEX + String(accZ) + END_INFO_INDICATOR;
      String hallInfo = HALL_EFFECT_INFO_INDICATOR + String(completedCount)+ END_INFO_INDICATOR;
      String pitchInfo = PITCH_INFO_INDICATOR + String(KalAngleX) + END_INFO_INDICATOR;
      String rollInfo = ROLL_INFO_INDICATOR + String(KalAngleY) + END_INFO_INDICATOR;
      
      
      Serial.println("Enviando dados para bluetooth celular");

      writeStringToBluetooth(START_PACKEGE_DATA);
      
      writeStringToBluetooth(girosInfo);
      writeStringToBluetooth(acceInfo);
      writeStringToBluetooth(hallInfo);
      writeStringToBluetooth(pitchInfo);
      writeStringToBluetooth(rollInfo);
      
      writeStringToBluetooth(END_PACKEGE_DATA);
      
      bluetooth.write(SUCCESS_REQUEST_INDICATOR);
    
    } 
    else 
    {
      
      Serial.print(request + "\n");
      sendErrorToBt(); 
    
    }
  } 
}

void writeStringToBluetooth(String information) {
  char* buf = (char*) malloc(sizeof(char)*information.length()+1);
  information.toCharArray(buf,information.length() + 1);
  bluetooth.write(buf);
  free(buf);
}

void sendErrorToBt() {
  bluetooth.write(ERROR_REQUEST_INDICATOR);
}

void ContarRodaTraseira()
{
   count++;
}
