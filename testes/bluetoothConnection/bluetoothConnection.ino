
#include <SoftwareSerial.h>

SoftwareSerial bluetooth(10,11);
//10 - tx  11 - rx (no arduino)

void setup() {
  Serial.begin(9600);
  bluetooth.begin(9600);
  
}

void sendInfoSerial() {
  Serial.write("DATA:");
  Serial.write("B:85");
  Serial.write("}");
}
 
void sendInfoBt() {
  bluetooth.flush();
  bluetooth.write("DATA:");
  bluetooth.write("A:12,15,16;");
  bluetooth.write("G:20,13,35;");
  bluetooth.write("H:5");
  bluetooth.write("}");
}

void loop() {  
  String request = "";
  
  if(bluetooth.available()) {
    while(bluetooth.available()) {
      char ch = bluetooth.read();
      request += ch;
      delay(10);
    }
    Serial.println(request);
    if(request == "SENDIT") {
      bluetooth.write("K");
      delay(1000);
      sendInfoBt();     
    } else bluetooth.print("R");
  }

  request = "";
  if(Serial.available()) {
    while(Serial.available()) {
        char ch = Serial.read();
        request += ch;
        delay(10);
    }
    
    if(request == "SENDIT") {
      Serial.write("K");
      delay(1000);
      sendInfoSerial();     
    } else Serial.write("R");
  }
}


