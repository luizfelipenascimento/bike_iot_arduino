
#include <Wire.h>
#include <Kalman.h>
#include <SoftwareSerial.h>


//INDENTIFICADORES - TODO E QUALQUER IDENTIFICADOR DEVE SER FORMADO POR UMA PALAVRA OU LETRA SEGUIDO POR ":"(DOIS PONTOS). EX.: "A:" , "C:" , "IDENT:"

//indicadores para requisicao
//R = indica um erro durante a requisicao, esse erro é enviado ao solicitante. Ocorre quando o arduino recebe alguma coisa, porém não é uma requisicao valida (algo diferente de SENDIT)
//K = indica que a requisição é valida, essa informação é enviada ao solicitante logo apos o recebimento da requisicao. (requisição = SENDIT) 

const char ERROR_REQUEST_INDICATOR = 'R';
const char SUCCESS_REQUEST_INDICATOR = 'K';

const String SENDIT_KEY = "SENDIT";
const String FIRST_RESET_KEY = "FIRSTRESET";

const String NO_ACCIDENT = "N";


//identificadores das informacoes que serao enviadas INDICADORES SEMPRE PRECISAM DE ':' (DOIS PONTOS APOS A STRING IDENTIFICADORA)
const String GYROSCOPE_INFO_INDICATOR = "G:";
const String ACCELEROMETER_INFO_INDICATOR = "A:";
const String HALL_EFFECT_INFO_INDICATOR = "H:";
const String ROLL_INFO_INDICATOR = "O:";
const String PITCH_INFO_INDICATOR = "P:";
const String LINEAR_ACELERATION_ACC = "LAC:";
const String AVERAGE_ACCELEROMETER_Z = "MAZ:";
const String AVERAGE_ACCELEROMETER_Y = "MAY:";
const String ACCIDENT_INIDICATOR = "ACDNT:";

//identificadores dos eixos: para acelerometro e giroscopio
const String Z_INDEX = "z:";
const String X_INDEX = "x:";
const String Y_INDEX = "y:";

//definicao das regras do pacote que sera enviado para o bluetooth

const String COMMA_SEPARATOR = ",";  //caso o sensor tenha um conjunto de dados atrelado a ele mesmo, e necessario utilizar a "," (virgula) para separar cada dado do sensor
                                    //como exemplo podemos pensar no sensor de acelerometro que possui 3 eixos XYZ - a informação ficaria: X,Y,Z (uso da virgula separando cada dado)  

const String END_INFO_INDICATOR = ";"; //final de cada conjuto de dados de cada sensor

const String START_PACKEGE_DATA = "DATA:"; // indicador do inicio do pacote que sera enviado SEMPRE UTILIZAR NO INICIO DO PACOTE (E A PRIMEIRA INFORMACAO QUE PRECISA SER ENVIADA)

const String END_PACKEGE_DATA = "}"; //indicador do final do pacote que sera enviado FINALIZADOR - SEMPRE UTILIZAR NO FINAL DO PACOTE (E A ULTIMA INFORMACAO QUE PRECISA SER ENVIADA)

//EXEMPLO DE PACOTES COMPLETOS COM DADOS: DATA:G:12,12,5;A:4,5,6;H:5;}

/*EXPLICACAO DETALHADA DE CADA PARTE DO PACOTE:
  
  DATA:  //(inicio do pacote)
  G:12,12,5; //(dados de um sensor que possui 3 eixos ) Identificador = G: / Eixos separados por = "," (virgula) / Finalizacao do conjuto de dados desse sensor =  ";" (ponto e virgula)
  A:4,5,6; //(dados de um sensor que possui 3 eixos ) Identificador = A: / Eixos separados por = "," (virgula) / Finalizacao do conjuto de dados desse sensor =  ";" (ponto e virgula)
  H:5;  //(dado de um sensor que possui somente 1 valor atralado a ele) Identificador = H: / valor / Finalizacao do conjuto de dados desse sensor = ";" (ponto e virgula)
  } // (finalizador do pacote)

*/

//REGRA DE COMUNICACAO COM BLUETOOTH 
//O ARDUINO SO IRA ENVIAR ALGUMA INFORMACAO PARA O CELULAR VIA BLUETOOTH CASO ELE TENHA RECEBIDO UMA REQUISICAO
// ESSA REQUISICAO E CARACTERIZADA POR UMA PALAVRA-CHAVE = (SENDIT). O ARDUINO IRA IDENTIFICAR QUE ESSA PALAVRA FOI ENVIADA
// E IRA COMECAR A PREPARAR OS DADOS PARA ENVIO.

 //TRATAMENTO DE ERROS DURANTE A REQUISICAO 
 //CASO O ARDUINO RECEBA ALGUMA INFORMACAO, MAS QUE NAO SEJA A PALAVRA-CHAVE (SENDIT) O ARDUINO IRA ENVIAR A LETRA (R) 
 // DE VOLTA AO SOLICITANTE INDICANDO QUE OCORREU UM ERRO DURANTE A REQUISICAO 

 //CASO O ARDUINO RECEBA A PALAVRA-CHAVE (SENDIT) CORRETAMENTE O ARDUINO IR ENVIAR A LETRA (K) DE VOLTA AO SOLICITANTE INDICANDO QUE A REQUISICAO FOI
 //RECEBIDA COM SUCESSO

 //RESUMO DAS PALAVRAS-CHAVE UTILIZADAS DURANTE A COMUNICACAO (TODAS AS PALAVRAS SAO MAIUSCULAS)
 /*
  SENDIT = utilizada para requisicao de dados no arduino, um solicitante precisa enviar essa palavra para que o arduino possa responder com os dados

  R = indica um erro durante a requisicao, esse erro é enviado ao solicitante. Ocorre quando o arduino recebe alguma coisa, porém não é uma requisicao valida (algo diferente de SENDIT)

  K = indica que a requisição é valida, essa informação é enviada ao solicitante logo apos o recebimento da requisicao. (requisição = SENDIT) 
 
 */

uint8_t i2c_data[14];
double accX, accY, accZ;
double gyroX, gyroY, gyroZ;

uint32_t timer;

Kalman KalmanX;
Kalman KalmanY;
Kalman KalmanZ;

double KalAngleX;
double KalAngleY;
double KalAngleZ;

double gyroXangle;
double gyroYangle;

const long INTERVALO_SEGUNDOS = 1000 * 2;

//Contador do sensor efeito hall
int pino_hall_pedal = 2;
int pino_hall_roda_traseira = 3;

static int countRoda = 0;
static int countPedal = 0;

long tempoInicial;
long tempoFinal;
int completedCountRoda = 0;
int completedCountPedal = 0;


String accidentStatus = NO_ACCIDENT;

const int WINDOW_MAX = 5;
double accZLimitArray[WINDOW_MAX];
double accYLimitArray[WINDOW_MAX];
int accIndex = 0;

double accZmedia = 0;
double accYmedia = 0;

SoftwareSerial bluetooth(11,10);

/************************************************************************
 * Setup Inicial com as Configurações para i2C.
 ************************************************************************/

void setup() {

  /* Inicializando a Serial para exibir mensagens de Debug */
  Serial.begin(115200);

  /* Inicializando o Barramento i2c para comunicação com a MPU6050 */
  Wire.begin();

  /*Inicializando bluetooth*/
  bluetooth.begin(9600);
  
#if ARDUINO >= 157
  Wire.setClock(400000UL); // Freq = 400kHz.
#else
  TWBR = ((F_CPU/400000UL) - 16) / 2; // Freq = 400kHz
#endif

  i2c_data[0] = 7;      /* 0x19 - Taxa de amostragem  8kHz/(7 + 1) = 1000Hz */
  i2c_data[1] = 0x00;   /* 0x1A - Desabilitar FSYNC, Configurar o Filtro de ACC 260Hz, Configurar Filtro de Gyro 256Hz, Amostragem de 8Khz */
  i2c_data[2] = 0x00;   /* 0x1B - Configurar o fundo de escala do Gyro ±250deg/s - Faixa */
  i2c_data[3] = 0x00;   /* 0x1C - Configurar o fundo de escala do Acelerômetro para ±2g - Faixa */

  /* Configirações do i2c*/
  while(i2cWrite(0x19, i2c_data, 4, false));

  /* PLL tenha como referência o gyro de eixo X, Desabilitando Sleep Mode */
  while(i2cWrite(0x6B, 0x01, true));

  /* */
  while(i2cRead(0x75, i2c_data, 1));

  if(i2c_data[0] != 0x68){
    Serial.print("Erro. Placa desconhecida\n");
    while(1){
      Serial.print("Erro. Conecte a MPU6050 no barramento i2c\n");
    }
  }

  /* Tempo de estabilização do Sensor MPU6050 */
  delay(100);

  /* 1 - Leitura dos dados de Acc XYZ */
  while(i2cRead(0x3B, i2c_data, 14));

  /* 2 - Organizar os dados de Acc XYZ */
  accX = (int16_t)((i2c_data[0] << 8) | i2c_data[1]); // ([ MSB ] [ LSB ])
  accY = (int16_t)((i2c_data[2] << 8) | i2c_data[3]); // ([ MSB ] [ LSB ])
  accZ = (int16_t)((i2c_data[4] << 8) | i2c_data[5]); // ([ MSB ] [ LSB ])

  /* 3 - Calculo de Pitch e Roll */  
  double pitch = atan(accX/sqrt(accY * accY + accZ * accZ)) * RAD_TO_DEG;
  double roll = atan(accY/sqrt(accX * accX + accZ * accZ)) * RAD_TO_DEG;

  /* 4 - Inicialização do Filtro de Kalman XY */
  KalmanX.setAngle(roll);
  KalmanY.setAngle(pitch);

  gyroXangle = roll;
  gyroYangle = pitch;
  
  timer = micros();

  pinMode (pino_hall_pedal, INPUT);  //Define o pino como entrada
  pinMode (pino_hall_roda_traseira, INPUT); //Define o pino como entrada
  
  tempoInicial = millis();
  tempoFinal = millis();
  attachInterrupt(digitalPinToInterrupt(pino_hall_pedal), ContadorPedal, CHANGE);
  attachInterrupt(digitalPinToInterrupt(pino_hall_roda_traseira), ContarRodaTraseira, CHANGE);

  //ContadorPedal
  //ContarRodaTraseira
  
  initializeValuesAcc(accZLimitArray, accZ);
  initializeValuesAcc(accYLimitArray, accY);
  
}


void loop() {

  String request = "";

  if(bluetooth.available()) {
    while(bluetooth.available()) {
      char ch = bluetooth.read();
      request += ch;
      delay(10);
    }

    if (request.indexOf(FIRST_RESET_KEY) != -1) {
      Serial.println("resetando informacoes");
      accidentStatus = NO_ACCIDENT;
      resetaContadores();
      for (int i = 0; i < WINDOW_MAX; i++) {
        accYLimitArray[i] = 0;
        accZLimitArray[i] = 0;
      }
    }
    
    if (request.indexOf(SENDIT_KEY) != -1) {
      
      //leitura do sensor efeito hall com intervalo
      while((tempoFinal - tempoInicial) <= INTERVALO_SEGUNDOS + 500) {
        tempoFinal = millis();
        if (tempoFinal - tempoInicial >= INTERVALO_SEGUNDOS) {
          
          //a contangem é sempre acrescentada quando o estado do sensor e modificado (ligar ou desligar) para ter somente o valor de um estado count foi dividido por 2
          
          completedCountRoda = countRoda / 2;
          completedCountPedal = countPedal / 2;
          
          Serial.println("contador: " + String(completedCountRoda)); 
          Serial.println("completedCountPedal: " + String(completedCountPedal));
          
          resetaContadores();
          
          tempoInicial = millis();
          break;
        }
      }
      
      /* Leitura dos Dados de Aceleração e Gyro do sensor MPU6050 */
      while(i2cRead(0x3B, i2c_data, 14));
    
      /*Aceleração*/
      accX = (int16_t)((i2c_data[0] << 8) | i2c_data[1]); // ([ MSB ] [ LSB ])
      accY = (int16_t)((i2c_data[2] << 8) | i2c_data[3]); // ([ MSB ] [ LSB ])
      accZ = (int16_t)((i2c_data[4] << 8) | i2c_data[5]); // ([ MSB ] [ LSB ])
    
      /*Giroscópio*/
      gyroX = (int16_t)((i2c_data[8] << 8) | i2c_data[9]); // ([ MSB ] [ LSB ])
      gyroY = (int16_t)((i2c_data[10] << 8) | i2c_data[11]); // ([ MSB ] [ LSB ])
      gyroZ = (int16_t)((i2c_data[12] << 8) | i2c_data[13]); // ([ MSB ] [ LSB ])
      
      /*
      Serial.print("AccXYZ"); Serial.print("\t");
      Serial.print(accX); Serial.print("\t");
      Serial.print(accY); Serial.print("\t");
      Serial.print(accZ); Serial.print("\n");
      Serial.print("GiroXYZ"); Serial.print("\t");
      Serial.print(gyroX); Serial.print("\t");
      Serial.print(gyroY); Serial.print("\t");
      Serial.print(gyroZ); Serial.print("\n");
      */  
    
      /******************* Filtro de Kalman *************************/
      
      /* Calculo do Delta Time */
      double dt = (double)(micros() - timer)/1000000;
      timer = micros();
    
      double pitch = atan(accX/sqrt(accY * accY + accZ * accZ)) * RAD_TO_DEG;
      double roll = atan(accY/sqrt(accX * accX + accZ * accZ)) * RAD_TO_DEG;
    
      /* Convertendo de Rad/Segundo para Graus/Segundo Calculo da Taxa angular baseado no Giroscópio */
      gyroXangle = gyroX / 131.0; //deg/s
      gyroYangle = gyroY / 131.0; //deg/s
    
      /* Estimativa de Ângulo nos Eixos X e Y usando Filtro de Kalman */
      KalAngleX = KalmanX.getAngle(roll, gyroXangle, dt);
      KalAngleY = KalmanY.getAngle(pitch, gyroYangle, dt);
    
      //Serial.println("Enviando dados para bluetooth celular");

      accYLimitArray[accIndex % WINDOW_MAX] = accY; 
      accZLimitArray[accIndex % WINDOW_MAX] = accZ;
      
      accIndex = (accIndex < WINDOW_MAX) ? accIndex + 1 : 0;

      accZmedia = calculoMedia(accZLimitArray);
      accYmedia = calculoMedia(accYLimitArray);

      Serial.println("accZmedia: " + String(accZmedia));
      Serial.println("accYmedia: " + String(accYmedia));
      
      if ((accZmedia <= 6000 && accYmedia >= 12500) || (accZmedia <= 6000 && accYmedia <= -12500)) {
        Serial.println("\n\n **** POSSIVEL ACIDENTE ****\n\n");
        accidentStatus = "Y";
      }
    
      //Serial.print(accZmedia); Serial.print("\t");
      //Serial.print(accYmedia); Serial.print("\n");  
      
      //Serial.print(accX); Serial.print("\t");
      //Serial.print(accY); Serial.print("\t");
      //Serial.print(accZ); Serial.print("\n");

      //CRIANDO PACOTE DE INFORMACAO COM GIROSCOPIO
      String girosInfo = GYROSCOPE_INFO_INDICATOR + X_INDEX + String(gyroX) + COMMA_SEPARATOR + Y_INDEX + String(gyroY) + COMMA_SEPARATOR + Z_INDEX + String(gyroZ) + END_INFO_INDICATOR;
      
      //CRIANDO PACOTE DE INFORMACAO COM ACELEROMETRO
      String acceInfo = ACCELEROMETER_INFO_INDICATOR + X_INDEX + String(accX) + COMMA_SEPARATOR + Y_INDEX + String(accY) + COMMA_SEPARATOR + Z_INDEX + String(accZ) + END_INFO_INDICATOR;

      //CRIANDO PACOTE DE INFORMACAO PARA O EFEITO HALL DO PEDAL
      String hallInfo = HALL_EFFECT_INFO_INDICATOR + String(completedCountRoda)+ END_INFO_INDICATOR;

      //CRIANDO PACOTE DE INFORMACAO PARA O VALOR DE PITCH
      String pitchInfo = PITCH_INFO_INDICATOR + String(KalAngleY) + END_INFO_INDICATOR;
      
      //CRIADNO PACOTE DE INFORMCAO PARA O VALOR DE ROLL
      String rollInfo = ROLL_INFO_INDICATOR + String(KalAngleX) + END_INFO_INDICATOR;


      //CRIANDO PACOTE DE INFORMACAO PARA MEDIA DOS ULTIMOS 5 VALORES DO EIXO Y DO ACELEROMETRO
      String mediaAccYInfo = AVERAGE_ACCELEROMETER_Y + String(accYmedia) + END_INFO_INDICATOR; //-> media accY 
      
      //CRIANDO PACOTE DE INFORMACAO PARA MEDIA DOS ULTIMOS 5 VALORES DO EIXO Z DO ACELEROMETRO
      String mediaAccZInfo = AVERAGE_ACCELEROMETER_Z + String(accZmedia) + END_INFO_INDICATOR; //-> media accZ 

      //CALCULANDO A VELOCIDADE LINEAR DO ACELEROMETRO
      double aceleracaoLinearAcc = sqrt((accX * accX) + (accY * accY) + (accZ * accZ));
      
      //CRIANDO PACOTE DE INFORMACAO PARA VELOCIDADE LINEAR DO ACELEROMETRO
      String linearAccInfo = LINEAR_ACELERATION_ACC + String(aceleracaoLinearAcc) + END_INFO_INDICATOR;

      String accidentInfo = ACCIDENT_INIDICATOR + String(accidentStatus) + END_INFO_INDICATOR;
      
      bluetooth.flush();
      
      writeStringToBluetooth(START_PACKEGE_DATA);

      //envidando informacoes do giroscopio e do acelerometro
      writeStringToBluetooth(girosInfo);
      writeStringToBluetooth(acceInfo);

      //TIRAR O COMENTARIO DA PROXIMA LINHA APOS RESOLVER O PROBLEMA COM O PINO 4 SIG - SV1
      writeStringToBluetooth(hallInfo);

      writeStringToBluetooth(pitchInfo); 
      writeStringToBluetooth(rollInfo);
      
      //enviando valores da media dos eixos y e z do acelerometro
      writeStringToBluetooth(mediaAccZInfo);
      writeStringToBluetooth(mediaAccYInfo); 

      //enviando informacao sobre a velocidade linear do 
      writeStringToBluetooth(linearAccInfo); 

      writeStringToBluetooth(accidentInfo);
      
      writeStringToBluetooth(END_PACKEGE_DATA);
      
      bluetooth.write(SUCCESS_REQUEST_INDICATOR);
      
      bluetooth.flush();

      //resetando accidentStatus
      accidentStatus = "N";

    } else {
      Serial.println("request content : "  + request);
      bluetooth.write(ERROR_REQUEST_INDICATOR);
    }
  }
}

double calculoMedia(double values[]) {
  double soma = 0;
  int valuesLength = sizeof(accZLimitArray) / sizeof(double);
  
  for (int i = 0; i < valuesLength; i++) {
    soma += values[i];
  }
  return soma / valuesLength; 
}

void initializeValuesAcc(double values[], double value) {
  int valuesLength = sizeof(accZLimitArray) / sizeof(double);
  for (int i = 0; i < valuesLength; i++) {
    values[i] = value;
  }
}

void writeStringToBluetooth(String information) {
  char* buf = (char*) malloc(sizeof(char)*information.length()+1);
  information.toCharArray(buf,information.length() + 1);
  bluetooth.write(buf);
  free(buf);
}

void resetaContadores() {
   countRoda = 0;
   countPedal = 0;
}

void ContarRodaTraseira()
{
   countRoda++;
}

void ContadorPedal()
{
   countPedal++;
}
